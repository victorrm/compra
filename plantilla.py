compra_habitual = ["patatas", "leche", "pan"]
compra_especifica = []

while True:
    producto = input("Productos a comprar (Presiona Enter para terminar): ")
    if not producto:
        break
    compra_especifica.append(producto)

listacompra = list(set(compra_habitual + compra_especifica))

print("\nLista de la compra:")
for item in listacompra:
    print(item)

print("Elementos habituales:", len(compra_habitual))
print("Elementos específicos:", len(compra_especifica))
print("Elementos en lista:", len(listacompra))
